# UT_HW_Models

Gimp file/images: Artwork Models for most hardwares compatible with Ubuntu Touch.


## Getting started
All the gimp files are located in the Phones, Tablets folders.

## License
All files/images are free to use/adapt as promotional material for the UBports project.  
Please note the original designs of phones and tablets and all pictures used as start of the project are the property of their respective owners.  
So I can't make the files Creative Commons cause I don't own the original design of the phones tablets used as input for the work.